#!/usr/bin/env python3

from PyPDF2 import PdfFileReader, PdfFileWriter
from tkinter import Tk
from tkinter.filedialog import askopenfilename

Tk().withdraw()
filename = askopenfilename() 
file = filename

pdf = PdfFileReader(file)

newPdf = PdfFileWriter()

for page in range(pdf.getNumPages()):
    current_page = pdf.getPage(page)
    newPdf.addPage(current_page)
    newPdf.addBlankPage()

finalPdf = filename + "_with_Blanks"
with open(finalPdf, "wb") as out:
     newPdf.write(out)
     print("created", finalPdf)
